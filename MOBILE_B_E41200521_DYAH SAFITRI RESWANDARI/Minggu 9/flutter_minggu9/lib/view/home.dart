import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';
import '/controller/demoController.dart';
import '/controller/purchase.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:get/get.dart';
import 'package:get/get_core/get_core.dart';

class HomePage extends StatelessWidget {
  final purchase = Get.put(Purchase());
  DemoController cart = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        ),
        bottomSheet: SafeArea(
          child: Card(
            elevation: 12.0,
            margin: EdgeInsets.zero,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              height: 65,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Stack(
                      children: [
                        Icon(Icons.add_shopping_cart_rounded,
                          size: 40, color: Colors.white),
                        Positioned(
                          right: 5,
                          child: Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.red,
                            ),
                            child: Center(
                              child: GetX<DemoController>(builder: (controller) {
                                return Text(
                                  '${controller.cartCount}',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 11.0),
                                );
                              }))
                          ))
                      ],
                    ),
                    GetX<DemoController>(builder: (controlller) {
                      return Text(
                        'Total Amount - ${controlller.totalAmount}',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                          fontSize: 16.0),
                      );
                    }),
                    IconButton(
                      onPressed: () => Get.toNamed('/cart',
                        arguments: 
                          "Home Page To Demo Page -> Passing arguments"),
                      icon: Icon(
                        Icons.arrow_forward_ios_rounded,
                        color: Colors.white,
                      ))
                  ],
                ),
              ),
            ),
          ),
      ),
      body: Container(
        child: GetX<Purchase>(builder: (controller) {
          return ListView.builder(
            itemCount: controller.products.length,
            itemBuilder: (context, index) =>Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    elevation: 2.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Column(
                      children: [
                        Image.network(
                          'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBIREhIREhISERIRGBEREREREhISEhIYGBgZGRgYGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QGhISHjErJCE0MTQ0ND8xNTQ6NDE0NDQxNDQ0MTQxNDc0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0MTQ0NDQ0Mf/AABEIALIBGwMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAAAwECBAUGB//EAEIQAAIBAgMECAIHBQgCAwAAAAECAAMRBBIhBTFBURMiYXGBkaGxBsEUMkJSktHwI2JygqIVFkNTssLS8TPhJGNz/8QAGgEAAwEBAQEAAAAAAAAAAAAAAAECAwQFBv/EACwRAAICAQIFAwQCAwEAAAAAAAABAhEDBBITITFBUXGRoRQyYYEisVLh8UL/2gAMAwEAAhEDEQA/AOnlkZI7LDLPmz60Rlhlj8sjLCxiMsMsflkWiGIyQyR9oWhYGfJDJH5YZIAIyQyR1oZYWMRlkZZoyyMsViM+SRlmnLIyR2BnyypSacsjLCwMxSQUmnJIKR2FGbLIyTTlkZYWFGUpINOaSkqUlWTRmNOVKTUUlSkdiozFJBSaikqUjsVGYpKmnNJSRkgmKjIUlSk1lJUpKsmjKacg05qKSpSOxNGQpIyTUUkZY7Jo9PaRljLQtMKNbF5ZGWNywywoNwrLDLG5YZYUG4TlhljcsMsVD3CssMsblhlhQbhOWGWOyyMsKDcJywyx2WRaFD3CssMsZlhlhQ7FZYZYzLIyxUFi7SMsblkZY6HuFZZUpHZYZYUFiCkqVmgrIKRgZisMs0FJUpGIzlZXLNGWVKxiEFZUrHlYZZRLM5SQUjysqVgAgpKFJpKypWNEmYpIyTQUkZZQj0NoWjcsMsxJ3CrQtG5YZYw3CrQtG5ZOSFBuE2haOyQ6OOg3CbSMsf0cOjhQbhGWGWP6OGSFBuM+WRlmgpDJFQbjPlkZZoySMsB7hGWGWOyyMkB7hOWRlj8kjJAe4TlkWjskgpAe4TlkFY7LIywHuElZBWPKyMsB7jOUkZI+0grALMxSQVjysqVlAIKypWaCkqVhYhBWRljisgrHYhBWRlj8sraVZNHoQskJLgSQs50znchYWTkjgskLNES5Cejk5I8LJCRoneI6OGSaQkt0cqhbzLkh0c15IdHHQt5k6OR0c2dHDo4UHEMXRyOjm3o5BSFD4hi6OBSazTkFJND3mTJIKTWUlSkRSmZCkjJNJSVKSSlMzFJBSaCsgrFZSkZ8kqUmgrKlYrKUjOUkFY4rIKydxSkIKyCI4iUIhvKTFkSpEYRKmLeXZS0qRGGVMe8aF5ZUiNtKGPeFFCJXLGGRHvDad0GWBnOFcy4rmefHOcrxM6IIlwROYKzSwrNNo5yXiZ1BLCcsVmlxWbnNY514M3iZ0hL6TmCs3OXFZv0Zos68EvEzowtMK1zLjEGaLNEh45Gy0LTH9JMn6SYcWIuHI15ZGWZPpMg4qHFiPhyNRUSCBMpxco2MkvNEpY5Go90oT2TIcXKNi5nLNHyWsUjWX7JQvMTYyUbGTJ50arDLwbWaULTA2MlDjDMnnNFhkdAtKlpzjjDKHGRcZlrBI6JaVZpzWx0q2Ok75PsWsEjolpQtOY2PlG2gOcpb32KWFnTLSpcTkttAc4ptprmC31YMw04KVB/1CXHHkfYexLqzsl5Q1Jxn2iAQtwCQSBpcgbzbxi32mo3uo7yBNFhyPsFwXVnbNQShqiefbblIb6qD+dfzmZ/iKgP8S/8ACrt7Cax02V9mS8uKPWS9z0xrCR0wnkn+KKI3F27kIv52iv72U/8ALqf0fnNVpMz7Gb1WFd0bl+L6f3XHe6y6/FqcA340HvaefNXBi1s7i31XVgT2XDad8uuN2eLXw7k8dWsP6tZ2vQ4P8WcC1eTvJHfX4sX7r/jQ37tZc/FVhmyPb+JPznnX2hgRqMLm7MzoPO5kNtfCEH/4pU/ZsQwHeSYvocP+LK+rl3kvY9MnxSSMwp1iOYAI8xAfFn7lXyX5zyh2jSBulILu0K5uXbH4bbVNSGNJXPH9nTHlYQegxePkFq231XsenHxWeCVfJLS4+KG3CnVPHRVM4R+JNxWmoIvbqemgF7Sr7frvcMtQjUWVFt5kGT9FDx8mn1EfPwegX4pY3GSoCtswIQWuLjjykf3sOpyVCBxAQ+xnm6W06iPUdKVQPUyhiyod17HrKeZ9Iz+18TranVueJFK45fYg9FDx8iWoVf6PQH4vspfJUyjeSEsPWWf4pcAE061jYhsgtY8b3nlMTVr1tKlOoxII6z5Ftx0AA3TeuJrhf2iFDoNalLLYdgGbd2wejxr/AKNZpN9Pg7TfFw4q/wDR/wAof3r3dSpr+6v5ziU2xjXFNVPA2ZmA/KC4bH3LEZSOHRiT9Jh7/wBlcWXZfB22+KLb6dbtsgNvIxa/FKnUJV/Bb3M4j09oAG7EDeSVReFrzPTwuKY/+Ugm4vdhr5b41o8Fc/7E8875Rfseib4nUb0qjvS3zkD4mU/4db8F/YzgLsmsDrV3cejZvffNSbEa9yzkcb0FIPcbkDyiek06Gs2Z9jfW+JW0yUmtxNQ5R6Ayn94K2/olI4WqX+UhNkYYAl1e+m9wgHgBpFvs3CncCvdW07+Nolg0/TaU55utoadv1f8AKHi5/wCMqdv1f8tPGqB7iZm2dQG6xHAdOT/pEr9Ew4OqDQXN6tQj2EtabB2iTxcy7oa/xBVJyinTv2VVb2m2jWxz6/R1t21FEw0cclEnosic8oDXtu+sCRIf4iq7xUPc1NbfK8mWmX/iC/YLUSX3Sf6OhUONAv8AR1PYtUX9RMlTF4xd+EqeHW9hLUdt4m+r4Yg7ukemtvwMTOzS2pRdQWqUlb7Sh8wHcSBfymMoSx/dBP0NY5t/SbXqeUr7fqIcrU8rcmaxHeLTI236x3CmP5ST7z2b4nCHe9EnuS/naYqtDZzHM3RE8ySPabQyY11xsynHK+mRHja2067kE1D1TdcqqtjYjgORMS+Mqtqar/jYe095T2Zg2AZKVF1O4jrA+ZknZWG0tQo79eovIzRazGuW1+xi9Hllzc/k+dvcm5JJ5kknzMrknvcTT2dT+umHB+6ti3kJwNo4zAG4p4ck8DmZF8hOrHn39Is5Mun2dZI4OUSCss7XNwMo5Ak285WdByBlhaEIxHoKezTUv0dAOAbXRKrDuzZpdNkEsFallOt9HFu/WbaWHrIOo9gON2a1+1YJUrDql2N/ulhfzPynLvl2aPTjijytfBA2XS+ylPTRs9cpbuuDHJsimozZKDHgvS518cqA+RlkwdR7EqxVebNc9100j12eeFNNdwqVKhbvIUTKU2u5ssS8CKNBCbdFhiRfd049LGTUwzj6qUgexGYDxK+lprGyGJHSFQNTkU1b28900vslMotTovbcGqVFPideczeVJ9TRYnXQ4gWogPX6MDh0C6+FwZFOsSDnrVLH7tNFtpxzG3lOm2zgnXGGpgjd0bu/rp7TSrWGtSojHkyhreBv6QeXwEcTs5GHqUVvZ3a9txp5j5X8hGYh1HWZsSu7KBTqW7NSQPQTc2KSx0xRGoDBm138wBFLVouMp6Ycy7oBbtAHyi3PrTK2drRD4pyosmIH3iVHW8LfOJxW0aaHRCToDmqA2O7VQN/ZeXarTS4Vq9jYdRrL4brxRrKerkrPbcxZmcd0FXgbbqrBcahsSlM79y5ct+Oq+wmlatRiP22ZdOq+e3trM7KSLBcUF5Zso8tR5mC0GtcpWK/ZzPTsfDOb8YNIS3G1xUG4qM2/SoQPEpb2mes9RdVLsbW0y7/SYGqKmpQrxBNSmD5WNotsQTqM+uls9O/hZZUcYnkR16WGqvlzu6ngoez+ICbpSphEYWckkcGqO1/ICYGe1syVAdMxYhhbna2sZ0tNdQS5PDLY+phtYWu5FXDJdVsOJAz1j85T+zja62t2ZiPG7aSKtYE9WiwJGt6aXPbYgzNXHFkI0H1Vy+YAmkUzOVc+Rrp0FBvUta3AEi/4jINCn9qoo42VVFvAm95gZVN8qi31uuxtbnoJKvUW3WRb6izP6jfK2vyZ714G1Vp36tQH95xT0iAlNrkkE/vWUeFtIx2cXLuSW3WQemhiw443L65j1Bpz+reWuhnJpvoVzquYAJ2ZantYSMw5Anvv53Ez18VTGpN92l1v4nfEfSadicjm+7iPNpaRjKVPqjbn1y2H8uvziqlamDrvG67fJTOZiMQX01A5XiZagYyzeEbXxeVs1Pqm98wUAg9hNyIqtjKlT69R3/idiPKZrwvHtV3RDyS6WXvIvK3heUTZa8Lyl4XgTZa8Lyt4XgM9PQRGbq00J33IIHiTadDDYioosoCA6dW637Ab3E4AxWIO66A8jv8AEy6tiRuYg/ysfnbwnNKF9aPRhmS6J+x6KqGe5cctXesV4cAfnFtikSwWogtfQUyAe8sTPPslU61KzC+8Go48LWmrDbNptbrBzvIZwpt2zNwilzZsssm/4r3Z1G2gtS4NdQN31UUfMHylaqKoujVHB1ugBv5KBM9HDUVDfVJBsAHuD3c5upU6aE2DaDT9rSI8mbv4zN7Y/aaxU5L+Ve5iOHqnQU6lzYhSylrduuktSw+KU2AqU7b7vTUehJnTzgW6+6x3qxbsFr2PgJeoerYpT7A9xfh1uNz3GS8r8IpYV5ZzkRyevXVTrcGozN3aXF4GlUAJL1MvAkBFHLkTOolTo1VWpUwTcWRHI3b+qhuT4SlaowayUqanddkQcrWsT7RcRt9CljVdTlrUf6pqO5ANwM17d5aMR3+qrsOYz1GtyvlBHhNpYkkVEBDWAKkWOgJ329jGLVRBqGXMRq1R31G62huLcr+Ebn+BrH+TnkYgDdnv9oln07Ad3pLPSq2X9mqAak1Kisbbr2y28I5sRTuCyG3WXMgBA33NhvHnL09oUwdOkBuRoChYDnmGndeLc+yDbHuznVsTUptlRaavoClNEF+0nL8ppp1HdFDqiX1Id0ynuGQ6x/T9JplZwDfNnU5Tz0W5HbYzl4naFKm5UuwI3kXb/SovumiuXJLmZSahzb5HSq4YhSzhWC2sNDfs4n0lKjql2KrZstkcZbG/AHQb+Vz2TkPtIOTaoAg1yZ9T+M2/W6UG06QKl3UBblVVc9jzLBbeQlLFLuQ88Ox1mpu2pRFUn7jNbjpcBZlxLZlsr1HvuUFMo4byRp3RH9r0zYK1ZyNwpJlHjoMxk08VXdh0WDf91nunidAPWOMWubFOcXyXP0Lph2y3ZqnVsSpqadt7DSY8U9NNcwubb2ZvYH1m59j4vEEdM1OmvALaow8r2850MP8AC+FS2a7txzlwPLT2hxYR6u/QnhZJcoql5Z5l8bT+qudgdSqA3HZmbX0nR2NsE1yXq02p0wBkGZwXN+O82tPUYbCUqeiU0Udg/NYx8QBoG/XpM56htVFV+TTHpFe6bv8ABhTYdBPq0kHC53+Z1mPG7IRtAgHIg29BOm+I/ePrMtSueDDxufnM4ynd2byx46qkeaxGxCL29xObXwLpvE9dUqk8R5iY64B32PgJ1QzS7nDl00H9p5NlIlJ2sThgdbCc2pRtOmMkzgnjcWZ7wkkSsoyJvC8iEAC8IXheAHqi/JEsd5ILXHgZRsp13ai1lsG46a3JlUd2AXo1t/AoU+J3mOo06gI6yU7m1wiXNu0b5xt0euuYsV+t1KYNt7Oqpv8AHdG56upC0wdQctO5bzYSxqFDq7OSbBQMqi/Hq7z5mWOJAF3yKBqCx1NuYb20kt+EUl5ZWkKlgTUIyncKamxJ0G7/AHS9Ok4zZ3pAXzG6sSw7FG/fxvMmJ2j0mlJCQMoL9RVtpfSxNvGJKltc4Ww0Skuc6i4HWvYdtxHtbXPkLck/42/2dOviagZV6ZSRrkQFdDwuLkct0vhqj3JGepbN/wCJ6iiw5jdftAmFEtoEd1e1y9lpk8xuAk4vGLRUAOjvc/ZRiBpuyi1h266ydt8kVxKtyfI6a4lkGUPkDXZmUtfNcWUn6o77D1k0sSipnYvYmxZ1RXOtuLAnfwFpwF23T3kMWF7dSmqi99BqTbWS22yxYUaOYtYm659RyAufM+EXBfdB9TBc0zs/TDcZANRfd1rGwFgOHbvlekOUqzU00NmZXYnTlutaw1nHoU8fV+ogQcQUpp6EXm5Ng4x/rVFpjkqEnwsI3CEerQllnLpFv4AVVFyufTVnZmpoLbiL39/CYm2qzMRTptUc6Eg1Hv3gNO9hfhSiCGrPUrNyZgqn528Z3sJh6dHqoiKvAKAPlrJlnhH7VZUcGWX3Ovk8QmzcdXB6qoN/WZKd79i7/GbMJ8GsQDWdx2U7EW7zPZGv3egPjeVatyU+AB+cyepyPlGkaR0eO7lb9WcnC/DeDpixpBzxZ2uffSaqeycKrZkpUlPMKCRHtX/dI8APnEviuw+szvI+rfubrHCPRL2NqZF3Ed1v+5V6ycSe6z/lOW+K7R/X69WJfFciO4K/5RrHY3JI6j1wNx9D8xM71+RHtOXUxZ39XytMlTHH9zzPymkcTM5Zoo7D1T2eazM9T9ZpxqmKY8vAkxDYk8z6/lNY4mYS1COpVr24H8R/OZnxI5frznPOJP3j5yDiT971/wDc1WMwlms0vX7R+vGKaqfvD0iGrHn6j84s1f1+jNFAxlkGuxPH2meprBqn6/Ri2aUlRlKViXWJYR7RbTRMwkhUJJlTGQEIQgB6D6UgBylRf7Zbh8/CLfHMxsootb7TMddLbjacs4WruyNNVDYtZhc2XvsT6TBxhHm2diyZZckmOZnIIetTRW1K0wNezT84sPh01LNUPcfnLnYZH1qlv5bfOaqGw6Q1dy3iU/2mJzgu/si1jyt9F+2ZTtdBupnsF1sO7SUO1qr6U1IPEqMzegsPKduhsqgpuFUnhnfN6FJ0qalfqrTA5g1E9VEylmguiN44MsvulXojyX9m4usbuGPG7t7TpYT4WFr1nfuprf11noMzcz/KSw/rAkFm/wC1y+ueZvPNqlSNI6PGncrfqIwWwcNT1yM5+86N1Z0kamugKgDha3+4TFY8gf51Y/1NLGuRoSwtzdbeV5i90nzdnVGMIqkkv0dFaw+yM34vzlvpNvs28fzFpyGxXFlX+YX94s7QUbqfiOr5aiCxNlcVI7IxgPH+tberSTiRz8mW3zvOE+1Tw6QHsZW/3n2martZhxcH/wDOnp4yo4GyJaiKPRtiFGmcDsugPqYiri0H+ID2Zk/5GeZfa1QbnYd4y+xiH2nUP2t/IkTVadmUtXFdD0lTHKDvXxZfYiZn2iuozoTy6Sl855w4w8/ID5mT/aD7gWA7ND6TRYDGWrTOtVxx+8LdhRvZhMj4r94fhQ+pac18STqSx79ZT6R2DyE1WOjnlqGzc2J/h8bD2MUapv8AWHgZlbEE8/14yhqnjKUDKWWzW1Y8T6ypqiZM/fILGVtI4hqNYShrd/r+czkyM0e0lzY41T2+ZlTUMUXkF46JchuYyC8VmkZoULcMzSpaVvC8YrAyCIXheAiDIkmRAR6/Op+73Ef+oBV/+oeS/KY3xQH2w3YAw9xFNjDwz94sR7Th4bPYeaK6nVVgPuf0GXWov+WjdwpKfMWnDbGtwPnFviWO8qfBPyj4LJ+pijvtWp/dI7Br63+UUaq7wvgM/wApwuk7/C3tILjn6IJSwryS9T+DunFHhTJ7uk/7in2hl0vUB7KlQH1nFz8jbwX3h0h5k+EpYUQ9SzqPtEnfc/xMfe8W20O/uFQexnOD25DtykH0kM9/t378/wA5Sxoh55eTW2KHJvO/qIp8QP1Y+4mUnu8IXmigjJ5ZMa1W/wCh8hKdIRuPkSJQwB7/ADtHRDk2TnPM+cMx7ZUmRmgKy1+33gbfu+bSvSEbiw7jKF/0YCtDNOyGbt/1Reb9ayL/AKvHQrG5hz9DKlh2esXfvhf9WhQtxct+tZUmReF4UKwvC8i8Lxism8i8LyIDJvC8iRARaErJgBMJEIAF4QkQA2AyAx5nzhCSajRwlTJhAohZUb4QgJkQEIQEiBAwhAREgwhAGVMDJhKJKysIQEEIQgAGRCEACRCEAJkQhAQQhCAAZEIQAIQhAAhCEACEIQAIQhAD/9k=',
                          fit: BoxFit.cover,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      controller.products[index].productName,
                                      style: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                    ),
                                    Text(
                                      controller.products[index].productDescription,
                                      style: TextStyle(
                                        color: Colors.grey[600],
                                        fontWeight: FontWeight.normal,
                                        fontSize: 12.0),
                                      ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ElevatedButton(
                                onPressed: () => cart.addToCart(controller.products[index]),
                                child: Text(
                                  'Shop Now',
                                  style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12.0),
                                )),
                              )
                          ],
                        ),
                      ],
                    ),
                  ),
                ));
        }),
      ),
    );
  }
}